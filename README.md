# ClassCalc Challenge 1

1. Method List
  - addList(input) // Input trigger function, Param: input value
  - calc() // caculate the result for all from inputted value.
  - findParenthesis() // find parenthesis from inputted value.
  - calcInsideParenthesis() // caculate the result for values inside parenthesis
  - roundFloatingPoint() // round the result with following number of length and remove javascript float calc overflow.
  - removeParenthesis // remove parenthesis from inputted value.
  - setError() // show Error if inputted value is impossible to caculate the result mathmatically.

2. Challenge Requirement Solution
  - Task1: All calculation modes were implemented. Test cases are tested and solved exactly.
  - Task2: Implemented Radian, Degree mode mathematically without any third party library. Line number: 85~114
  - Task3: Implemented resin functionality, Line number: 69~83
  